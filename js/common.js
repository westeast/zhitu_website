/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {


var app=__webpack_require__(1);

$(function(){
  app.addActive();
  app.headFixed();

});



/***/ }),
/* 1 */
/***/ (function(module, exports) {

/***根据url动态添加header和三级页面的active**/
function addActive(){
  var strPath=window.document.location.pathname;
  //console.log(strPath)
  var paths=strPath.split('/'),
      currentUrl=paths[paths.length-1],
      id,child;
  //console.log(paths)
  if(currentUrl==''){
    $('[data-id="index"]').addClass('active');
  }else{
    id=paths.length<=4?currentUrl.split('.')[0]:paths[2];
    //console.log(id);

    $('[data-id='+id+']').addClass('active');

  }
}

/**页面头部固定**/
function headFixed() {

  var gotoTop_html = '<i class="icon_top"></i>';

  $('#toTop').append(gotoTop_html);

  $('#toTop').on('click',function () {
    $(this).find('.icon_top').css('background-position','-749px 0');

    $('html,body').animate({scrollTop:0},700);
  });

  window.onscroll=function(){
    var top=document.documentElement.scrollTop
        ||document.body.scrollTop;
    if(top>=80){
      $('#header').addClass('shadow');
      $('#toTop').show(100);

    }else{
      $('#header').removeClass('shadow');
      $('#toTop').hide(200).find('.icon_top').removeAttr('style');
    }
  }
}



/***tabber**/
function tabber(sel,index) {
  $('#'+sel).on('click','a',function (e) {
    e.preventDefault();
    //console.log(this);
    var parent=$(this).parent(),
        id=$(this).attr('href');
    parent.addClass('active').siblings().removeClass('active');
    $(id).addClass('active').siblings().removeClass('active');
  })
}

/***首页加载新闻**/

function indexNews() {
  $.getJSON('https://www.easy-mock.com/mock/599e7128059b9c566dce484f/zhitu/index',function (pager) {
    var html='';
    var delay=.3;
    //console.log(pager)
    $.each(pager.data,function(i,news){
      html+=`
        <div class="news-item clearfix wow fadeInUp animated" data-wow-delay="${(delay+=.1)}s">
          <a href="news/details/specific.html">
            <div class="leftPicture">
              <img src="${news.pic}" alt="">
              <div class="labelOrgen">${news.classify}</div>
            </div>
            <div class="rightText">
              <div class="title">${news.title}</div>
              <div class="readData">
                <div>
                  <span>阅读</span>
                  <img src="images/news/reader.png" alt="">
                  <span>${news.readNumber}</span>
                </div>
                <div>
                  <span>发布日期</span>
                  <img src="images/news/time.png" alt="">
                  <span>${news.time}</span>
                </div>
              </div>
              <div class="textInfo">
                ${news.text}
              </div>
              <div class="moreBtn">查看详情</div>
            </div>
          </a>
        </div>
      `;
      $('#indexNews').html(html);
    })
  })
}

/**智慧旅游环形闪烁***/
function scaleable(i) {
  var smCircle=document.querySelectorAll('.letterCircle');
  $(smCircle[i]).addClass('animate').siblings().removeClass('animate');
  //console.log(i);
  i++;
  if(i>9){
    i=0;
  }
  setTimeout(scaleable.bind(this,i),3000);
}

/***news异步加载数据和分页**/
function loadNews(pno,url) {
  $.getJSON(url,{'pno':pno},function (pager) {
    var html='';
    var delay=.3;
    //console.log(pager)
    $.each(pager.data,function (i,news) {
      html+=`
        <div class="news-item clearfix wow fadeInUp animated" data-wow-delay="${(delay+=.1)}s">
          <a href="details/specific.html">
            <div class="leftPicture">
              <img src="${news.pic}" alt="">
              <div class="labelOrgen">${news.classify}</div>
            </div>
            <div class="rightText">
              <div class="title">${news.title}</div>
              <div class="readData">
                <div>
                  <span>阅读</span>
                  <img src="../images/news/reader.png" alt="">
                  <span>${news.readNumber}</span>
                </div>
                <div>
                  <span>发布日期</span>
                  <img src="../images/news/time.png" alt="">
                  <span>${news.time}</span>
                </div>
              </div>
              <div class="textInfo">
                ${news.text}
              </div>
              <div class="moreBtn">查看详情</div>
            </div>
          </a>
        </div>
      `;
    });
    $('#newsLists').html(html);

    var pageHtml='';
    if(pager.pno-2>0){
      pageHtml+=`<li class="pageItem"><a href="${pager.pno-2}">${pager.pno-2}</a></li>`;
    }
    if(pager.pno-1>0){
      pageHtml+=`<li class="pageItem"><a href="${pager.pno-1}">${pager.pno-1}</a></li>`;
    }
    pageHtml+=`<li class="pageItem active"><a href="${pager.pno}">${pager.pno}</a></li>`;
    if(pager.pno+1<=pager.pageCount){
      pageHtml+=`<li class="pageItem"><a href="${pager.pno+1}">${pager.pno+1}</a></li>`;
    }
    if(pager.pno+1<=pager.pageCount){
      pageHtml+=`<li class="pageItem"><a href="${pager.pno+2}">${pager.pno+2}</a></li>`
    }

    $('#pagition').html(pageHtml);

    var index=$('#pagition>li').index($('#pagition>li.active'));
    if(index==0){
      $('.pages>.prev').addClass('disabled')
    }else{
      $('.pages>.prev').removeClass('disabled')
    }
    if(index==pager.pageCount-1){
      $('.page>.next').addClass('disabled');
    }else{
      $('.page>.next').removeClass('disabled');
    }
  })
}


/**招聘信息**/
function jobLoads() {
  $.getJSON('https://www.easy-mock.com/mock/599e7128059b9c566dce484f/zhitu/job',function (jobs) {
    var jobHtml='',
        reqireHtml='';
    $.each(jobs.data,function (i,job) {
      jobHtml+=`
        <div class="tab">
          <a href="#tab${job.id}">
            <span>${job.job}</span> <b></b>
          </a>
        </div>
      `;

      reqireHtml+=`
        <div class="tab-pane wow fadeInUp animated" id="tab${job.id}">
          <div class="head">
            <div class="jobName">${job.job}</div>
            <div class="email">请投递：<a href="mailto:hongjun.ye@ztemap.com">hongjun.ye@ztemap.com</a></div>
          </div>
          <div class="jobRequire">
            <div class="title">
              <div class="line"></div>
              <div>岗位描述：</div>
            </div>
            <div class="requireContent">
              ${job.jobDescrip}
            </div>
          </div>
          <div class="jobRequire">
            <div class="title">
              <div class="line"></div>
              <div>岗位要求：</div>
            </div>
            <div class="requireContent">
              ${job.jobRequire}
            </div>
          </div>
        </div>
      `;
    })

    $('#myTab').html(jobHtml);
    $('#myTabContent').html(reqireHtml);
    $('#myTab .tab:first').addClass('active');
    $('#myTabContent .tab-pane:first').addClass('active');

    var jobs=$('#myTab .tab'),
        pHeight=$('.jobs').height();
        cHeight=$('.tab').height(),
        mb=parseInt($('.tab').css('margin-bottom')),
        maxShow = parseInt(pHeight / (cHeight + mb));
    if(jobs.length>maxShow){
      $('.jobArrow').show();
    }else{
      $('.jobArrow').hide();
    }
  })
}


/**招聘岗位滚动**/
var job= {
  i: 0,//初始active
  added:0,//保存已添加active个数
  id: null,//招聘岗位信息对应的id
  pHeight: null,//显示岗位的模块高度
  cHeight: null,//每个岗位方块的高度
  mb: null,//每个方块的下外边局
  maxTop:null,//最大位移高度
  nowTop:null,//当前位移
  maxShow: null,//最多显示的岗位数
  jobNum:null,//全部岗位
  canAuto: true,//是否自动轮播
  index: null,//循环的timer
  init: function () {
    this.pHeight=$('.jobs').height();

    this.autoAdd();
    //点击跳转到当前岗位，从新开始循环
    $('.jobsPost').on('click','a',function(e){
      e.preventDefault();
      var parent=$(this).parent(),
          id=$(this).attr('href');
      parent.addClass('active').siblings().removeClass('active');
      $(id).addClass('active').siblings().removeClass('active');
      job.jobTab();
    });

    //鼠标移入/移出
    $('.jobsPost').on('mouseover',function () {
      this.canAuto = false;
    }.bind(this));
    $('.jobsPost').on('mouseout',function () {
      this.canAuto = true;
    }.bind(this));

    //鼠标移入/移除箭头
    $('.jobArrow').on('mouseover',function(){

      var baseUrl=$(this).find('img').attr('src'),
          originUrl=baseUrl.split('/')
          imgName=originUrl[originUrl.length-1].split('.'),
          hoverUrl='';
      imgName[0]=imgName[0]+'-'+imgName[0];
      originUrl[originUrl.length-1]=imgName.join('.');
      hoverUrl=originUrl.join('/');

      $(this).find('img').attr('src',hoverUrl);

    }).on('mouseout',function(){
      var originUrl=$(this).find('img').attr('src').split('/'),
          imgName=originUrl[originUrl.length-1].split('.'),
          baseUrl='';

      imgName[0]=imgName[0].split('-')[0];
      originUrl[originUrl.length-1]=imgName.join('.');
      baseUrl=originUrl.join('/')

      $(this).find('img').attr('src',baseUrl);
    })

    //点击箭头前进/后退一个
    $('.jobArrow').on('click','a',function(e) {
      e.preventDefault();
      var jobs=$('#myTab .tab');

      var nowI=$('#myTab>.tab').index($('#myTab>.active'));
      job.nowTop=parseFloat($('#myTab').css('top'));

      if(jobs.length>15){
        var pageN=Math.floor((jobs.length)/(job.maxShow+1));

        if($(this).parent().hasClass('prev')){
          nowI-=job.maxShow+1;
          if(nowI<0){
            nowI=pageN*(job.maxShow+1);
          }
        }

        if($(this).parent().hasClass('next')){
          nowI+=job.maxShow+1;
          if(nowI>jobs.length-1){
            nowI=0;
          }
        }
        $('#myTab').css('top', '-' + (job.cHeight + job.mb) * (nowI) + 'px');
      }else {

        if ($(this).parent().hasClass('prev')) {

          nowI--;
          if (nowI < 0) {
            nowI = jobs.length - 1;
          }

          if (nowI - job.maxShow >= 0 && nowI - job.maxShow <= job.jobNum - 1 - job.maxShow) {
            $('#myTab').css('top', '-' + (job.cHeight + job.mb) * (nowI - job.maxShow) + 'px');
          } else if (nowI <= job.jobNum - 1 - job.maxShow) {
            if (job.nowTop !== 0) {
              $('#myTab').css('top', '-' + (job.cHeight + job.mb) * (nowI) + 'px');
            }
          }
        }

        if ($(this).parent().hasClass('next')) {
          nowI++;
          if (nowI > jobs.length - 1) {
            nowI = 0;
          }

          if (nowI <= job.jobNum - 1 - job.maxShow) {
            $('#myTab').css('top', '-' + (job.cHeight + job.mb) * (nowI) + 'px');
          } else if (nowI - job.maxShow >= 0 && nowI - job.maxShow <= job.jobNum - 1 - job.maxShow) {
            if (job.nowTop !== job.maxTop) {
              $('#myTab').css('top', '-' + (job.cHeight + job.mb) * (nowI - job.maxShow) + 'px');
            }

          }
        }
      }

      job.jobArrow(jobs,nowI);
    });


  },
  autoAdd: function () {//自动轮播
    this.index = setTimeout(
        function () {
          if (this.canAuto) {
            this.add(1);//启动添加一个active
          } else {
            this.autoAdd()
          }
        }.bind(this), 6000
    )
  },
  add: function (n) {
    clearInterval(this.index);
    this.index = null;

    var jobs=$('#myTab .tab');
    this.jobNum=jobs.length;

    this.cHeight=$('.tab').height();
    this.mb=parseInt($('.tab').css('margin-bottom'));
    this.maxShow = parseInt(this.pHeight / (this.cHeight + this.mb)) - 1;
    this.maxTop=-(this.jobNum-1-this.maxShow)*(this.cHeight+this.mb);
    this.nowTop=parseFloat($('#myTab').css('top'));

    $(jobs[this.i]).addClass('active').siblings().removeClass('active');
    this.id=$(jobs[this.i]).find('a').attr('href');
    $(this.id).addClass('active').siblings().removeClass('active');

    if(this.i<=this.jobNum-1-this.maxShow){
      $('#myTab').css('top', '-' + (this.cHeight + this.mb) * (this.i) + 'px');
    }else{
      if(this.nowTop>this.maxTop){
        $('#myTab').css('top', '-' + (this.cHeight + this.mb) * (this.i-this.maxShow)  + 'px');
      }
    }


    if(this.i>this.jobNum-1){
      $('#myTab').css('top', 0)
    }

    this.i+=n;
    if(this.i>this.jobNum-1){
      this.i=0;
    }
    this.index=setInterval(
        this.addActive.bind(this,n),this.jobNum
    )
  },
  addActive:function(n){//添加一个active
    this.added++;
    if(this.added+1==this.jobNum){
      clearTimeout(this.index);
      this.index=null;
      this.added=0;

      this.autoAdd();
    }
  },
  jobTab:function () {//岗位切换
    clearTimeout(this.index);
    this.index=null;
    this.i=$('#myTab>.tab').index($('#myTab>.active'));

  },
  jobArrow:function(jobs,nowI){//前进/后退岗位
    clearTimeout(this.index);
    this.index=null;

    $(jobs[nowI]).addClass('active').siblings().removeClass('active');
    this.id=$(jobs[nowI]).find('a').attr('href');
    $(this.id).addClass('active').siblings().removeClass('active');
    this.i=$('#myTab>.tab').index($('#myTab>.active'));

    this.autoAdd();
  }
}


/**联系我们总分公司信息***/
function companyInfo() {
  $.getJSON('https://www.easy-mock.com/mock/599e7128059b9c566dce484f/zhitu/company',function (companys) {
    var html='';
    $.each(companys.data,function (i,company) {
      html+=`
        <div class="company wow fadeInUp animated" data-wow-delay=".3s" id="${company.id}">
          <div class="company_name">${company.name}</div>
          <div class="company-item">
            <div class="icon"><img src="../images/contact/ct-tel.png" alt=""></div>
            <div class="title">联系电话：</div>
            <div class="message">${company.tel}</div>
          </div>
          <div class="company-item">
            <div class="icon"><img src="../images/contact/ct-e-mail.png" alt=""></div>
            <div class="title">邮箱地址：</div>
            <div class="message">${company.email}</div>
          </div>
          <div class="company-item">
            <div class="icon"><img src="../images/contact/ct-web.png" alt=""></div>
            <div class="title">公司官网：</div>
            <div class="message">${company.web}</div>
          </div>
          <div class="company-item">
            <div class="icon"><img src="../images/contact/ct-address.png" alt=""></div>
            <div class="title">公司地址：</div>
            <div class="message">${company.address}</div>
          </div>
        </div>
      `
    })
    $('#company').html(html);
    var id=$('.companyMap .address.active').children('a').attr('href');
    $(id).addClass('active');
    $('.company .message').each(function () {
      if($(this).html()==''){
        $(this).parent().hide();
      }
    })

  })
}

module.exports={
  addActive,
  headFixed,
  scaleable,
  tabber,
  indexNews,
  loadNews,
  jobLoads,
  job,
  companyInfo
}

/***/ })
/******/ ]);